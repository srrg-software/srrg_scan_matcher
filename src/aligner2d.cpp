#include "aligner2d.h"

namespace srrg_scan_matcher {

  Aligner2D::Aligner2D() {
    _solver=new Solver2D();
    _iterations = 10;
    _correspondence_finder = 0;
  };

  Aligner2D::~Aligner2D() {
    if (_solver)
      delete _solver;
  };

  void Aligner2D::setSolver(Solver2D* solver_) {
    if (_solver) {
      delete _solver;
    }
    _solver=solver_;
    if (_correspondence_finder)
      _correspondence_finder->setSolver(solver_);
  }

  void Aligner2D::align(const Eigen::Isometry2f& initial_guess, const Eigen::Matrix3f& guess_information) {
    if (! _solver->reference() || ! _solver->current()) {
      throw std::runtime_error("fatal, either reference or current clouds not set");
    }
    _solver->setPrior(initial_guess, guess_information);
    _solver->setT(initial_guess);
    _correspondence_finder->init();
    _solver->setReferencePointsHint(_correspondence_finder->indicesReference());
    for (int i = 0; i < _iterations; ++i) {
      _correspondence_finder->compute();
      _solver->optimize(_correspondence_finder->correspondences(), i==_iterations-1);
    }
    // to be done, compute statistics
    
  }

}
