#include "cloud2d_aligner.h"

namespace srrg_scan_matcher {
  Cloud2DAligner::Cloud2DAligner(Projector2D* _projector){
    _iterations = 30; //aligner iterations
    setReference(0);
    setCurrent(0);
    _correspondence_finder=0; //define it in useProjectiveCorrespondenceFinder or useNNCorrespondenceFinder
    setProjector(_projector);

    _inlier_distance = 0.05; //Validator2D field
    _minInliersRatio = 0.5;
    _minNumCorrespondences = 50;
  }

  Cloud2DAligner::~Cloud2DAligner() {
  }

  void Cloud2DAligner::setProjector(Projector2D* projector_) {
    ProjectiveCorrespondenceFinder2D *pcorr= dynamic_cast<ProjectiveCorrespondenceFinder2D*>(correspondenceFinder());
    if (pcorr) {
      //set projector in projective correspondence finder
      pcorr->setProjector(projector_);
    }
    ProjectiveValidator2D::setProjector(projector_);
  }

  void Cloud2DAligner::useProjectiveCorrespondenceFinder() {
    NNCorrespondenceFinder2D *nncorr= dynamic_cast<NNCorrespondenceFinder2D*>(correspondenceFinder());
    if (!correspondenceFinder() || nncorr){
      if (nncorr){
	//if NNCorrespondenceFinder2D was used, delete and create a projective one
	delete nncorr;
      }
      ProjectiveCorrespondenceFinder2D* finder=new ProjectiveCorrespondenceFinder2D;
      finder->setProjector(ProjectiveValidator2D::projector());
      Aligner2D::setCorrespondenceFinder(finder);
    }
  }

  void Cloud2DAligner::useNNCorrespondenceFinder() {

    ProjectiveCorrespondenceFinder2D *pcorr= dynamic_cast<ProjectiveCorrespondenceFinder2D*>(correspondenceFinder());
    if (!correspondenceFinder() || pcorr){
      if (pcorr){
	//if ProjectiveCorrespondenceFinder2D was used, delete and create a NN one
	delete pcorr;
      }
      NNCorrespondenceFinder2D* finder=new NNCorrespondenceFinder2D;
      //finder->setMaxSquaredDistance(10);
      finder->setMaxSquaredDistance(0.5*0.5);
      Aligner2D::setCorrespondenceFinder(finder);
    }
  }


  void Cloud2DAligner::compute(const Eigen::Isometry2f& prior_mean){
  
    if (! _reference || ! _current){
      throw std::runtime_error("Reference of current cloud not defined in Cloud2DAligner");
      return;
    }

    _initial_guess = prior_mean;

    Eigen::Matrix3f prior_omega;
    prior_omega.setIdentity();
    float alpha1=10;
    float alpha2=10;
    float alpha3=10;
    Eigen::Vector3f prior=srrg_core::t2v(prior_mean);
    prior_omega(0,0)=alpha1/(1e-3 + fabs(prior.x()));
    prior_omega(1,1)=alpha2/(1e-3 + fabs(prior.y()));
    prior_omega(2,2)=alpha3/(1e-3 + fabs(prior.z()));

    float d = prior_mean.translation().norm()+0.5;
    _correspondence_finder->setMaxSquaredDistance(d*d);
    Aligner2D::align(prior_mean, prior_omega);
    //Aligner2D::align();
    std::cerr << "Initial guess" << srrg_core::t2v(prior_mean).transpose() << std::endl;
    std::cerr << "aligner->T()" << srrg_core::t2v(T()).transpose() << std::endl;
  }

  bool Cloud2DAligner::validate(){
  
    if (! _reference || ! _current){
      throw std::runtime_error("Reference of current cloud not defined in Cloud2DAligner");
      return false;
    }

    Cloud2D temp_current(*_current);
    temp_current.transformInPlace(T());
    ProjectiveValidator2D::setCurrent(&temp_current);
    ProjectiveValidator2D::compute();

    int num_correspondences=numCorrespondences();
    int num_inliers=numInliers();
    std::cerr << "Num Correspondences Validator2D: " << num_correspondences << std::endl;
    std::cerr << "Num Inliers Validator2D: " << num_inliers << std::endl;
  
    float inliers_ratio = 0;
    if (num_correspondences)
      inliers_ratio = (float)num_inliers/(float)num_correspondences;
    std::cerr << "Inliers Ratio: " << inliers_ratio << std::endl;

    if (num_inliers && inliers_ratio>_minInliersRatio && num_correspondences>_minNumCorrespondences)
      return true;
    else
      return false;
  }

  Eigen::Matrix3f Cloud2DAligner::alignInformationMatrix(){
    //Eigen::Matrix3f inf = solver()->informationMatrix();
    //int num_correspondences=correspondenceFinder()->correspondences().size();
    //std::cerr << "solver num_correspondences: " << num_correspondences << std::endl;
    //std::cerr << "solver inliers: " << solver()->inliers() << std::endl;
    //std::cerr << "solver inliers error: " << solver()->inliersError() << std::endl;
    //if (num_correspondences)
    //  inf = inf / (float) num_correspondences;

    Eigen::Matrix3f inf = 1e4* Eigen::Matrix3f::Identity();
    inf(2,2) = 1000;

    return inf;
  }

  void Cloud2DAligner::logAlign(int vid, int othervid){
    char closures_logfilename[1024];
    sprintf(closures_logfilename, "log-%05d.dat", vid);
    std::ofstream log_stream;
    log_stream.open(closures_logfilename, std::ofstream::out | std::ofstream::app);

    //logging initial guess and final alignment
    Eigen::Vector3f igv(srrg_core::t2v(_initial_guess));
    Eigen::Vector3f resv(srrg_core::t2v(T()));
    log_stream << othervid << " " << igv.transpose() << " " << resv.transpose() << std::endl;

    //logging correspondences
    log_stream << correspondenceFinder()->correspondences().size() << " ";
    for (size_t s=0; s<correspondenceFinder()->correspondences().size(); s++)
      log_stream << correspondenceFinder()->correspondences()[s].first << " " << correspondenceFinder()->correspondences()[s].second << " ";
    log_stream << std::endl;
  }
  
}
