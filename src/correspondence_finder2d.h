#pragma once

#include "projector2d.h"
#include "solver2d.h"

namespace srrg_scan_matcher {

  /*!
     Processing object that computes a list of correspondences.
     It needs to be invoked by an Aligner object, within the ICP loop.

     It is coupled with a Solver Object that it uses to
     - get the current and the reference clouds
     - get the current transform

     To use this object you should first initialize the class (once at the beginning of the icp iterations),
     after having set the reference and the current cloud in the solver.

     \code{.cpp}
     Solver2d* s=...
     CorrespondenceFinder2D* cfinder=...
     cfinder->setSolver(s);
     s->setReference(ref)
     s->setCurrent(cur);
     cfinder->init();

     for (int i=0; i< iterations; i++) {
	cfinder->compute(); // comptes the correspondences based on the current transform in the solver;
	const CorrespondenceVector& corr=cfinder->correspondences(); // this returns a vector of pairs of indices.
	for (size_t j=0; j<corr.size() j++){
	   int ref_idx=corr[j].first;
	   int curr_idx=corr[j].second;
	}
     }
     \endcode

     THIS IN AN  ABSTRACT BASE CLASS THAT IS SPECIALIZED IN DERIVED ONES
   */

  class CorrespondenceFinder2D {
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
    CorrespondenceFinder2D(Solver2D* solver = NULL);
    ~CorrespondenceFinder2D() {};
    virtual void compute() = 0;
    virtual void init() = 0;
    inline const CorrespondenceVector& correspondences() const { return _correspondences; }
    void drawCorrespondences(Eigen::Isometry2f T) const;
    inline void setSolver(Solver2D* solver_) {_solver = solver_;}
    inline const Solver2D* solver() const { return _solver;}
    inline Solver2D* solver() {return _solver;}
    inline float minNormalCos() const {return _min_normal_cos;}
    inline void setMinNormalCos(float mnc) {_min_normal_cos=mnc;}
    inline float maxSquaredDistance() const {return _max_squared_distance;}
    inline void setMaxSquaredDistance(float msd) {_max_squared_distance=msd;}
    inline const srrg_core::IntVector& indicesCurrent() const { return _indices_current; }
    inline const srrg_core::IntVector& indicesReference() const { return _indices_reference; }

  protected:
    Solver2D* _solver;
    float _max_squared_distance;
    float _min_normal_cos;
    CorrespondenceVector _correspondences;
    srrg_core::IntVector _indices_current;
    srrg_core::IntVector _indices_reference;
  };
}
