#pragma once 
#include <srrg_messages/laser_message.h>

#include "nn_correspondence_finder2d.h"
#include "projective_correspondence_finder2d.h"
#include "tracker2d.h"

namespace srrg_scan_matcher {
  using namespace srrg_core;

  //Manages the Cloud2D Tracker with higher level info coming from laser messages.
  class LaserMessageTracker: public Tracker2D {
  public:
    LaserMessageTracker();

    inline void setProjector(Projector2D* projector) {_projector = projector;}
    inline void setFrameSkip(int frame_skip){_frame_skip = frame_skip;}
    inline void setMaxMatchingRange(double max_matching_range){_max_matching_range = max_matching_range;}
    inline void setMinMatchingRange(double min_matching_range){_min_matching_range = min_matching_range;}
    inline void setNumMatchingBeams(int num_matching_beams){_num_matching_beams = num_matching_beams;}
    inline void setMatchingFov(double matching_fov){_matching_fov = matching_fov;}
    inline void setOdomWeights(float wx, float wy, float wtheta) {_odom_weights << wx, wy, wtheta; }

    inline void setLaserTranslationThreshold(double laser_translation_threshold) {_laser_translation_threshold = laser_translation_threshold;}
    inline void setLaserRotationThreshold(double laser_rotation_threshold) {_laser_rotation_threshold = laser_rotation_threshold;}

    void setCorrespondenceFinderType(std::string cfinder);
    bool compute(LaserMessage* las);
    inline const size_t& skippedFrames() const {return _skipped_frames;}
    
  protected:

    void initializeProjectorFromParameters(LaserMessage* las);
    
    int _frame_count;
    int _frame_skip;
    bool _has_guess;

    double _max_matching_range;
    double _min_matching_range;
    int _num_matching_beams;
    double _matching_fov;

    Eigen::Vector3f _odom_weights;

    double _laser_translation_threshold;
    double _laser_rotation_threshold;

    Projector2D* _projector;

    Eigen::Isometry2f _previous_odom;

    size_t _skipped_frames = 0;
    
  };



}
