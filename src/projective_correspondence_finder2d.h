#pragma once

#include "projector2d.h"
#include "correspondence_finder2d.h"


namespace srrg_scan_matcher {
  /**
     Specialization of a correspondence finder that uses a projective approach
     That is, it generates virtual scans and compares the same "beams" of the
     virtual scan.

     It requires a projector to function; 
     The parameters of the projector determine the angle between the beam, the minimum and maximum
     range and so on;
   */
  class ProjectiveCorrespondenceFinder2D: public CorrespondenceFinder2D {
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
    ProjectiveCorrespondenceFinder2D(Solver2D* solver = NULL, Projector2D* projector = NULL);
    ~ProjectiveCorrespondenceFinder2D() {};
    virtual void compute();
    virtual void init();
    inline Projector2D* projector() { return _projector;}
    inline const Projector2D* projector() const {return _projector;}
    inline void setProjector(Projector2D* projector_) {_projector=projector_;}
  private:
    Projector2D* _projector;
    srrg_core::FloatVector _projected_reference_ranges;
    srrg_core::FloatVector _projected_current_ranges;
  };
}
