#include "projective_validator2d.h"
#include <stdexcept>
#include "projector2d.h"

namespace srrg_scan_matcher {
  ProjectiveValidator2D::ProjectiveValidator2D(Projector2D* projector_) {
    _projector = projector_;
  }
  
  ProjectiveValidator2D::~ProjectiveValidator2D(){}

  void ProjectiveValidator2D::compute() {
    if (! _reference) {
      throw std::runtime_error("reference not set");
    }
    if (! _current) {
      throw std::runtime_error("current not set");
    }
    if (! _projector){
      throw std::runtime_error("projector not set");
    }

    _projector->project(_reference_ranges, 
			_reference_indices, 
			Eigen::Isometry2f::Identity(), 
			*_reference);

    _projector->project(_current_ranges, _current_indices, Eigen::Isometry2f::Identity(), *_current);
  
    int size = std::min(_reference_indices.size(), _current_indices.size());
    _num_inliers=0;
    _num_outliers=0;

    _num_correspondences=0;
    float mean_dist=0;
    for (int c = 0; c < size; ++c) {
      int& ref_idx = _reference_indices[c];
      int& curr_idx = _current_indices[c];
      float diff = 0;

      if (ref_idx >= 0 && curr_idx >= 0){
	++_num_correspondences;
	diff = std::fabs(_current_ranges[c] - _reference_ranges[c]);

	mean_dist += diff;

	if (diff < _inlier_distance)
	  ++_num_inliers;
	else
	  ++_num_outliers;
      }
    }

    if (_num_correspondences) {
      mean_dist /= _num_correspondences;
      _bad_points_ratio= (float)_num_outliers/(float)_num_correspondences;
    } else {
      _bad_points_ratio=1.0f;
    }
  }
}
