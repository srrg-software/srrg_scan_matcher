#include <stdexcept>
#include <iostream>

#include "srrg_path_map/path_map.h"
#include "nn_correspondence_finder2d.h"

namespace srrg_scan_matcher {
  using namespace std;

  NNCorrespondenceFinder2D::NNCorrespondenceFinder2D(Solver2D* solver):
    CorrespondenceFinder2D(solver){
    setResolution(0.1);
  }

  void NNCorrespondenceFinder2D::compute() {
    if (0 && (! _solver->reference()  || !_solver->current())) {
      //throw std::runtime_error("NNCorrespondenceFinder2D::init(): either the current or the reference are not set");
      return;
    } 

    if(!_solver->current()->size() || ! _solver->reference()->size()){
      cerr << __PRETTY_FUNCTION__ <<": Either ref or current have 0 points. Something bad will happen.";
      return;
    }
    _indices_current.resize(_solver->current()->size());
    _correspondences.resize(_solver->current()->size());
    int k=0;
    //cerr << "A " << _solver->current()->size()  << " " << _solver->reference()->size()  << endl;
    for (size_t curr_idx=0; curr_idx<_solver->current()->size(); curr_idx++) {
      RichPoint2D pt_curr=_solver->current()->at(curr_idx);
      pt_curr.transformInPlace(_solver->T());
      Eigen::Vector2i coords=w2m(pt_curr.point());
      if (!inside(coords))
	continue;
      int ref_idx=_dmap_calculator.indicesMap().at<int>(coords.y(), coords.x());
      if (ref_idx<0)
	continue;
      const RichPoint2D pt_ref=_solver->reference()->at(ref_idx);
      if((pt_curr.point() - pt_ref.point()).squaredNorm() < _max_squared_distance &&
	 pt_curr.normal().dot(pt_ref.normal()) > _min_normal_cos) {
	_indices_current[k]=curr_idx;
	_correspondences[k]=std::make_pair(ref_idx,curr_idx);
	k++;
      }
    }
    //cerr << "SS" << endl;
    if (k) {
      _indices_current.resize(k);
      _correspondences.resize(k);
    } else {
      _correspondences.clear();
      _indices_current.clear();
     }
  }

  void NNCorrespondenceFinder2D::init() {
    if (! _solver) {
      throw std::runtime_error("fatal, solver not set");
    }
    _correspondences.clear();
    _indices_current.clear();
    _indices_reference.clear();
    // compute the bounding box of the current cloud;
    if (! _solver->reference()  || !_solver->current()) {
      throw std::runtime_error("NNCorrespondenceFinder2D::init(): either the current or the reference are not set");
      return;
    } 

    if (! _solver->reference()->size()  || !_solver->current()->size()) {
      cerr << "NNCorrespondenceFinder2D::init(): either ref or current have 0 points. Something bad will happen.";
    } 

    Eigen::Vector2f lower, upper;
    //std::cerr << _solver->reference() << " size: " << _solver->reference()->size() << std::endl;
    _solver->reference()->computeBoundingBox(lower, upper);
    // if no bbox abort
    if (lower.x()>upper.x() || lower.y()>upper.y()) {
      return;
    }
    _offset = lower;

    Eigen::Vector2f cloud_sizes=upper-lower;
    Eigen::Vector2i image_sizes(ceil(cloud_sizes.x()*_inverse_resolution)+16, 
				ceil(cloud_sizes.y()*_inverse_resolution)+16);
    _input_index_image.create(image_sizes.y(), image_sizes.x());
    _input_index_image=-1;
    
    int k = 0;
    _indices_reference.resize(_solver->reference()->size());
    for (size_t i=0; i<_solver->reference()->size(); i++) {
      const RichPoint2D& p=_solver->reference()->at(i);
      Eigen::Vector2i coords = w2m(p.point());
      if (! inside(coords))
	continue;
      int& idx = _input_index_image.at<int>(coords.y(), coords.x());
      if (idx>-1)
	continue;
      idx=i;
      _indices_reference[k]=i;
      k++;
    }
    if (k)
      _indices_reference.resize(k);
    else
      _indices_reference.clear();

    _dmap_calculator.setMaxDistance(_max_squared_distance*_inverse_resolution*_inverse_resolution);
    _dmap_calculator.setOutputPathMap(_distance_map);
    _dmap_calculator.setIndicesImage(_input_index_image);
    _dmap_calculator.init();

    _dmap_calculator.compute();
  }
}
