#pragma once

#include "cloud2d_trajectory.h"
#include "cloud2d_aligner.h"
#include "merger2d.h"
#include "cloud_processor.h"


namespace srrg_scan_matcher {
  using namespace srrg_core;
  
  class Cloud2DWithTrajectoryMerger {
  public:
    Cloud2DWithTrajectoryMerger();
    virtual ~Cloud2DWithTrajectoryMerger(){};

    inline void setReference(Cloud2DWithTrajectory* reference) {
      _reference=reference;
    }
    inline void setCurrent(Cloud2DWithTrajectory* current) {
      _current=current;
    }

    //Merges Current into Reference and leaves the merged cloud on the "current" frame 
    void compute();
    //Computes difference between reference and current and cancels points from reference without adding new points
    void prune();
    

    inline void setProjector(Projector2D* projector) {
      _projector = projector;
      _merger->setProjector(_projector);
      _c2DAligner->setProjector(_projector);
    }
    inline void setInliersDistance(float inliers_distance) {_inliers_distance = inliers_distance;}
    inline void setMinInliersRatio(float min_inliers_ratio) {_min_inliers_ratio = min_inliers_ratio;}
    inline void setMinNumCorrespondences(int min_num_correspondences) {_min_num_correspondences = min_num_correspondences;}
    inline void setMergingDistance(float merging_distance) {_merging_distance = merging_distance;}
    inline void setProjectiveMerge(bool projective_merge) {_merger->setEnableProjectiveMerge(projective_merge);}
    inline void setVoxelizeResolution(float voxelize_resolution){_voxelize_resolution = voxelize_resolution;}

    inline Eigen::Isometry2f transform() {return _initial_guess;}
    
    inline Cloud2DWithTrajectory* cloudMerged() {return _cloud_merged;}
  private:
    Projector2D* _projector;
    Cloud2DAligner* _c2DAligner;
    Merger2D* _merger;

    //Clouds
    Cloud2DWithTrajectory* _reference;
    Cloud2DWithTrajectory* _current;
    Cloud2DWithTrajectory* _cloud_merged;    

    Eigen::Isometry2f _initial_guess;
    
    //Aligner parameters
    float _inliers_distance;
    float _min_inliers_ratio;
    int _min_num_correspondences;

    float _merging_distance;
    float _voxelize_resolution;

  };



}

