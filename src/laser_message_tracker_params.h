#pragma once
#include "laser_message_tracker.h"

namespace srrg_scan_matcher {

  class LaserMessageTrackerParams {
  public:
    LaserMessageTrackerParams(LaserMessageTracker* tracker_ = NULL);

    void applyParams();

    inline void setTracker(LaserMessageTracker* tracker_) {_tracker = tracker_;}
    inline void setVerbose(bool verbose_) {_verbose = verbose_;}
    inline bool verbose() {return _verbose;}
    inline void setBpr(double bpr_) {_bpr = bpr_;}
    inline double bpr() {return _bpr;}
    inline void setIterations(int iterations_) {_iterations = iterations_;}
    inline int iterations() {return _iterations;}
    inline void setInlierDistance(double inlier_distance_) {_inlier_distance = inlier_distance_;}
    inline double inlierDistance() {return _inlier_distance;}
    inline void setMinCorrespondencesRatio(double min_correspondences_ratio_) {_min_correspondences_ratio = min_correspondences_ratio_;}
    inline double minCorrespondencesRatio() {return _min_correspondences_ratio;}
    inline void setLocalMapClippingRange(double local_map_clipping_range_) {_local_map_clipping_range = local_map_clipping_range_;}
    inline double localMapClippingRange() {return _local_map_clipping_range;}
    inline void setLocalMapClippingTranslationThreshold(double local_map_clipping_translation_threshold_) {_local_map_clipping_translation_threshold = local_map_clipping_translation_threshold_;}
    inline double localMapClippingTranslationThreshold() {return _local_map_clipping_translation_threshold;}
    inline void setVoxelizeResolution(double voxelize_res_) {_voxelize_res = voxelize_res_;}
    inline double voxelizeResolution() {return _voxelize_res;}
    inline void setMergingDistance(double merging_distance_) {_merging_distance = merging_distance_;}
    inline double mergingDistance() {return _merging_distance;}
    inline void setMergingNormalAngle(double merging_normal_angle_) {_merging_normal_angle = merging_normal_angle_;}
    inline double mergingNormalAngle() {return _merging_normal_angle;}
    inline void setProjectiveMerge(bool projective_merge_) {_projective_merge = projective_merge_;}
    inline bool projectiveMerge() {return _projective_merge;}
    inline void setDumpFilename(std::string dump_filename_) {_dump_filename = dump_filename_;}
    inline std::string dumpFilename() {return _dump_filename;}
    inline void setTrackerDamping(double tracker_damping_) {_tracker_damping = tracker_damping_;}
    inline double trackerDamping() {return _tracker_damping;}
    inline void setFrameSkip(int frame_skip_) {_frame_skip = frame_skip_;}
    inline int frameSkip() {return _frame_skip;}
    inline void setMaxMatchingRange(double max_matching_range_) {_max_matching_range = max_matching_range_;}
    inline double maxMatchingRange() {return _max_matching_range;}
    inline void setMinMatchingRange(double min_matching_range_) {_min_matching_range = min_matching_range_;}
    inline double minMatchingRange() {return _min_matching_range;}
    inline void setNumMatchingBeams(int num_matching_beams_) {_num_matching_beams = num_matching_beams_;}
    inline int numMatchingBeams() {return _num_matching_beams;}
    inline void setMatchingFov(double matching_fov_) {_matching_fov = matching_fov_;}
    inline double matchingFov() {return _matching_fov;}
    inline void setOdomWeights(const Eigen::Vector3i& odom_weights_) {_odom_weights = odom_weights_;}
    inline Eigen::Vector3i& odomWeights() {return _odom_weights;}
    inline void setLaserTranslationThreshold(double laser_translation_threshold_) {_laser_translation_threshold = laser_translation_threshold_;}
    inline double laserTranslationThreshold() {return _laser_translation_threshold;}
    inline void setLaserRotationThreshold(double laser_rotation_threshold_) {_laser_rotation_threshold = laser_rotation_threshold_;}
    inline double laserRotationThreshold() {return _laser_rotation_threshold;}

    inline void setCorrespondenceFinderType(std::string correspondence_finder_type_) {_correspondence_finder_type = correspondence_finder_type_;}
    inline std::string correspondenceFinderType() {return _correspondence_finder_type;}
  protected:

    LaserMessageTracker* _tracker;
  
    bool _verbose;
    double _bpr;
    int _iterations;
    double _inlier_distance;
    double _min_correspondences_ratio;
    double _local_map_clipping_range;
    double _local_map_clipping_translation_threshold;
    double _voxelize_res;
    double _merging_distance, _merging_normal_angle;
    bool _projective_merge;
    std::string _dump_filename;
    double _tracker_damping;
    int _frame_skip;
    double _max_matching_range;
    double _min_matching_range;
    int _num_matching_beams;
    double _matching_fov;
    Eigen::Vector3i _odom_weights;
    double _laser_translation_threshold;
    double _laser_rotation_threshold;
    std::string _correspondence_finder_type;

  };
}
