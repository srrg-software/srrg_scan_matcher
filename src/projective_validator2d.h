#pragma once
#include "validator2d.h"
#include "projector2d.h"

namespace srrg_scan_matcher {
  /**
     Specialized class of a validator that uses a scan comparison
     to determine how good are two clouds

     An example on how to initialize it follows
     \code{.cpp}
     ProjectiveValidator2D * validator;
     validator->setProjector(new Projector2D);
     \endcode

   */
  class ProjectiveValidator2D : public Validator2D {
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

  public:
    ProjectiveValidator2D(Projector2D* projector_ = NULL);
    virtual ~ProjectiveValidator2D();
    virtual void compute();
    inline Projector2D* projector() { return _projector;}
    inline const Projector2D* projector() const {return _projector;}
    inline void setProjector(Projector2D* projector_) {_projector=projector_;}
  private:
    Projector2D* _projector;
    srrg_core::IntVector _reference_indices, _current_indices;
    srrg_core::IntVector _reverse_reference_indices, _reverse_current_indices;
    srrg_core::FloatVector _reference_ranges, _current_ranges;
  };
}
