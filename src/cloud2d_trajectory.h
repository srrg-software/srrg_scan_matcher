#pragma once

#include "cloud2d.h"
#include "srrg_messages/laser_message.h"

using namespace std;

namespace srrg_scan_matcher {

  struct Cloud2DWithTrajectory: public Cloud2DWithPose{
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
    
    inline const Eigen::Isometry2f& lastPushedPose() const {return _lastPushedPose;};
    inline const std::vector<Eigen::Isometry2f>& trajectory() const {return _trajectory;};

    const Eigen::Isometry2f& waypoint(size_t i) const;
    const Eigen::Isometry2f globalWaypoint(size_t i) const;
    
    srrg_core::LaserMessage* laserWaypoint(size_t i) const; 

    void pushWaypoint(srrg_core::LaserMessage* laser=0);
    void drawWaypoint(size_t i);
    void draw(bool draw_normals = false,
	      bool draw_pose_origin = false,
	      bool draw_points = true,
	      bool use_fans=false);

    //adds waypoints of cloud_to_add into the current cloud (e.g., when merging clouds)
    void addTrajectory(Cloud2DWithTrajectory* cloud_to_add);
    //replaces the whole trajectory. setPose() should be used accordingly
    void setTrajectory(const std::vector<Eigen::Isometry2f>& trajectory){_trajectory = trajectory;};
    //expresses the waypoints wrt the newOriginPose (usefull when transformInPlace is done)
    void changeTrajectoryOrigin(const Eigen::Isometry2f& newOriginPose);

    void save(ostream& os) const;
    bool load(istream& is);

  protected:
    // Eigen::Isometry2f _pose; //This is inherited from Cloud2DWithPose
    // Trajectory contains a set of waypoints relative to _lastPushedPose
    // xk_n_k, ..., xk_2_k, xk_1_k, I
    std::vector<Eigen::Isometry2f> _trajectory;
    std::vector<srrg_core::LaserMessage*> _laserScans;

    // We save _lastPushedPose since global _pose could change during optimization
    Eigen::Isometry2f _lastPushedPose;
  };
}
