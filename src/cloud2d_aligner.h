#pragma once

#include "aligner2d.h"
#include "projective_validator2d.h"
#include "projective_correspondence_finder2d.h"
#include "nn_correspondence_finder2d.h"

namespace srrg_scan_matcher {
  class Cloud2DAligner: public Aligner2D,
                        public ProjectiveValidator2D{
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

    Cloud2DAligner(Projector2D* _projector=NULL);
    virtual ~Cloud2DAligner();
    inline const Cloud2D* reference() const { return _reference; }
    inline Cloud2D* reference() { return _reference;}

    inline void setReference(Cloud2D* reference_) { 
      _reference=reference_;
      Aligner2D::setReference(reference_); 
      ProjectiveValidator2D::setReference(reference_);
    }

    inline const Cloud2D* current() const { return _current; }
    inline Cloud2D* current() { return _current;}

    inline void setCurrent(Cloud2D* current_) {
      _current=current_;
      Aligner2D::setCurrent(current_); 
      ProjectiveValidator2D::setCurrent(current_);
    }
    
    inline const Projector2D* projector() const { return ProjectiveValidator2D::projector(); }
    void setProjector(Projector2D* projector_);

    void useProjectiveCorrespondenceFinder();
    void useNNCorrespondenceFinder();
  
    //Parameters for alignment validation
    inline void setInliersDistance(float inliersDistance_){ProjectiveValidator2D::setInlierDistance(inliersDistance_);}
    inline void setMinInliersRatio(float minInliersRatio_){_minInliersRatio = minInliersRatio_;}
    inline void setMinNumCorrespondences(int minNumCorrespondences_){_minNumCorrespondences = minNumCorrespondences_;}

    void compute(const Eigen::Isometry2f& prior_mean = Eigen::Isometry2f::Identity());
    bool validate();
    
    Eigen::Matrix3f alignInformationMatrix();

    void logAlign(int vid, int othervid);

  private:
    Cloud2D *_reference, *_current;
  
    Eigen::Isometry2f _initial_guess;
    Eigen::Matrix3f _align_information_matrix;

    float _minInliersRatio;
    int _minNumCorrespondences;
  };
}
