#include <GL/gl.h>
#include "cloud2d_trajectory.h"
#include "srrg_system_utils/stream_helpers.h"

namespace srrg_scan_matcher{
  using namespace srrg_core;

  const Eigen::Isometry2f& Cloud2DWithTrajectory::waypoint(size_t i) const 
  {
    if (i<_trajectory.size())
      return _trajectory[i];
    else
      throw std::runtime_error("Not existing waypoint");
  }

  const Eigen::Isometry2f Cloud2DWithTrajectory::globalWaypoint(size_t i) const 
  {
    if (i<_trajectory.size()){
      return _pose*_trajectory[i];
    }else
      throw std::runtime_error("Not existing waypoint");
  }

  srrg_core::LaserMessage* Cloud2DWithTrajectory::laserWaypoint(size_t i) const 
  {
    if (i<_laserScans.size())
      return _laserScans[i];
    else
      throw std::runtime_error("Not existing laser waypoint");
  }


  void Cloud2DWithTrajectory::pushWaypoint(srrg_core::LaserMessage* laser){
    if (!_trajectory.size()){
      //First waypoint
      Eigen::Isometry2f waypoint = Eigen::Isometry2f::Identity();
      _trajectory.push_back(waypoint);
      _laserScans.push_back(laser);
      _lastPushedPose = _pose;
    }else{
      //Get relative transf between _pose and _lastPushedPose
      Eigen::Isometry2f offset = _pose.inverse() * _lastPushedPose;
      //Compute relative transfs between old waypoints and new waypoint
      for (size_t i = 0; i < _trajectory.size(); i++){
	_trajectory[i] = offset * _trajectory[i];
      }
      //Relative transf of new waypoint with itself is the identity
      Eigen::Isometry2f newWaypoint = Eigen::Isometry2f::Identity();
      _trajectory.push_back(newWaypoint);
      _laserScans.push_back(laser);
      //All this transf are with respect to this pose
      _lastPushedPose = _pose;
    }
  }

  void Cloud2DWithTrajectory::drawWaypoint(size_t i){
    glPushAttrib(GL_COLOR|GL_POINT_SIZE);
    glPointSize(3.0f);
    glColor3f(0.5,0,1);
    glBegin(GL_POINTS);
    Eigen::Isometry2f globalwp = globalWaypoint(i);
    glVertex3f(globalwp.translation().x(), globalwp.translation().y(), 0.0f);
    glEnd();
    glPopAttrib();
  }

  void Cloud2DWithTrajectory::draw(bool draw_normals,
				   bool draw_pose_origin,
				   bool draw_points,
				   bool use_fans) {
    if (draw_points)
      Cloud2DWithPose::draw(draw_normals, draw_pose_origin, use_fans);
    
    for (size_t i=0; i < _trajectory.size(); i++){
      drawWaypoint(i);
    }
  }

  void Cloud2DWithTrajectory::addTrajectory(Cloud2DWithTrajectory* cloud_to_add){

    std::vector<Eigen::Isometry2f> trajectory_temp = _trajectory;
    Eigen::Isometry2f relative_pose = _pose.inverse()*cloud_to_add->pose();
    for (size_t i = 0; i < cloud_to_add->trajectory().size(); i++){
      Eigen::Isometry2f newWaypoint = relative_pose*cloud_to_add->waypoint(i);
      bool already_in_cloud = false;
      for (size_t j = 0; j < trajectory_temp.size(); j++){
	Eigen::Isometry2f wp = trajectory_temp[j];

	Eigen::Isometry2f wp_distance = wp.inverse()*newWaypoint;
	Eigen::Vector3f wp_vector = t2v(wp_distance);
	//Do not add waypoint if there is already one sufficiently close
	if (wp_distance.translation().norm()<0.1 && fabs(wp_vector[2])<M_PI_4){
	  already_in_cloud = true;
	  break;
	}
      }
      if (!already_in_cloud){
	_trajectory.push_back(newWaypoint);
	srrg_core::LaserMessage* laser = cloud_to_add->laserWaypoint(i);
	_laserScans.push_back(laser);
      }
    }

  }


  void Cloud2DWithTrajectory::changeTrajectoryOrigin(const Eigen::Isometry2f& newOriginPose){
    Eigen::Isometry2f relative_pose = newOriginPose.inverse()*pose();
    for (size_t i = 0; i < _trajectory.size(); i++){
      Eigen::Isometry2f newWaypoint = relative_pose*_trajectory[i];
      _trajectory[i] = newWaypoint;
    }    
  }


  void Cloud2DWithTrajectory::save(ostream& os) const {
    Cloud2DWithPose::save(os);
    size_t s=_trajectory.size();
    StreamHelpers::writeBinary(os,s);
    for (size_t i=0; i<s; i++){
      Eigen::Isometry2f wp=_trajectory[i];
      Eigen::Vector3f wpv = t2v(wp);
      StreamHelpers::writeBinary(os,wpv.x());
      StreamHelpers::writeBinary(os,wpv.y());
      StreamHelpers::writeBinary(os,wpv.z());
    }

    s=_laserScans.size();
    StreamHelpers::writeBinary(os,s);
    for (size_t i=0; i<s; i++){
      srrg_core::LaserMessage* laser = _laserScans[i];
      StreamHelpers::writeBinary(os,laser->minAngle());
      StreamHelpers::writeBinary(os,laser->maxAngle());
      StreamHelpers::writeBinary(os,laser->angleIncrement());
      StreamHelpers::writeBinary(os,laser->minRange());
      StreamHelpers::writeBinary(os,laser->maxRange());
      StreamHelpers::writeBinary(os,laser->timestamp());

      StreamHelpers::writeBinary(os,laser->ranges().size());
      for (size_t r=0; r<laser->ranges().size(); r++)
	StreamHelpers::writeBinary(os,laser->ranges()[r]);
    }
  }
  
  bool Cloud2DWithTrajectory::load(istream& is) {
    Cloud2DWithPose::load(is);

    size_t s;
    StreamHelpers::readBinary(is,s);
    _trajectory.resize(s);
    std::cerr << "Trajectory size: " << s << std::endl;
    for (size_t i=0; i<s; i++){
      Eigen::Vector3f wpv;
      StreamHelpers::readBinary(is,wpv.x());
      StreamHelpers::readBinary(is,wpv.y());
      StreamHelpers::readBinary(is,wpv.z());

      Eigen::Isometry2f wp=v2t(wpv);
      _trajectory[i] = wp;

    }

    StreamHelpers::readBinary(is,s);
    _laserScans.resize(s);
    for (size_t i=0; i<s; i++){
      srrg_core::LaserMessage* laser = new srrg_core::LaserMessage;
      float minAngle, maxAngle, angleIncrement, minRange, maxRange;
      double timestamp;
      StreamHelpers::readBinary(is,minAngle);
      StreamHelpers::readBinary(is,maxAngle);
      StreamHelpers::readBinary(is,angleIncrement);
      StreamHelpers::readBinary(is,minRange);
      StreamHelpers::readBinary(is,maxRange);
      StreamHelpers::readBinary(is,timestamp);
      
      laser->setMinAngle(minAngle);
      laser->setMaxAngle(maxAngle);
      laser->setAngleIncrement(angleIncrement);
      laser->setMinRange(minRange);
      laser->setMaxRange(maxRange);
      laser->setTimestamp(timestamp);
      size_t nranges;
      StreamHelpers::readBinary(is,nranges);
      std::vector<float> ranges(nranges);
      for (size_t r=0; r<nranges; r++)
	StreamHelpers::readBinary(is,ranges[r]);
      laser->setRanges(ranges);

      _laserScans[i] = laser;
    }
    
    return (is.good());
  }

}
