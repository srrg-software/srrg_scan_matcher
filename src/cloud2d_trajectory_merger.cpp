
#include "cloud2d_trajectory_merger.h"

namespace srrg_scan_matcher {
  
  Cloud2DWithTrajectoryMerger::Cloud2DWithTrajectoryMerger(){

    //Init Projector
    float fov = 4.53349; 
    int num_ranges = 1040;
    _projector = new Projector2D;
    _projector->setFov(fov);
    _projector->setNumRanges(num_ranges);
  
    //Init Merger
    _merger = new Merger2D(_projector);
    _merger->setEnableProjectiveMerge(true);
    
    //Init Cloud Aligner
    _c2DAligner=new Cloud2DAligner(_projector);
    _c2DAligner->useNNCorrespondenceFinder();

    _inliers_distance = 0.05;
    _min_inliers_ratio = 0.8;
    _min_num_correspondences = 100;

    _merging_distance = 0.1;
    _voxelize_resolution = 0;
    
    _c2DAligner->setInliersDistance(_inliers_distance);
    _c2DAligner->setMinInliersRatio(_min_inliers_ratio);
    _c2DAligner->setMinNumCorrespondences(_min_num_correspondences);
    
  
  }

  /*
  void Cloud2DWithTrajectoryMerger::compute(Eigen::Isometry2f& transform){

    if (!_reference || !_current){
      std::cerr << "You must provide two clouds" << std::endl;
      return;
    }
    
    
    //We avoid modify the original clouds
    _cloud_merged = new Cloud2DWithTrajectory;
    Cloud2DWithTrajectory* cloud_to_merge = new Cloud2DWithTrajectory;
    *_cloud_merged = *_reference;
    *cloud_to_merge = *_current;

    //Align current wrt reference using transform as i.g.
    _c2DAligner->setReference(_cloud_merged);
    _c2DAligner->setCurrent(cloud_to_merge);
    _c2DAligner->compute(transform);

    bool success_a = _c2DAligner->validate();
    if (success_a){
      //apply the result of the alignment
      Eigen::Isometry2f result_a = _c2DAligner->T();
      _initial_guess = result_a;
    }else {
      //apply the given transform for initial guess
      _initial_guess = transform;
    }
    cloud_to_merge->transformInPlace(_initial_guess);
      
  
    size_t wp_count = _cloud_merged->trajectory().size(); //only merge using wpoints from current
    //Both cloud trajectories expressed in reference origin.
    //Consistent since we did not change cloud_to_merge pose
    _cloud_merged->addTrajectory(cloud_to_merge); 
    for (size_t wp = wp_count; wp < _cloud_merged->trajectory().size(); wp++){
      Eigen::Isometry2f wp_transf = _cloud_merged->waypoint(wp);
      std::cerr << "Wp transf: " << t2v(wp_transf).transpose() << std::endl;

      //Get cloud points seen from the waypoint.
      FloatVector ranges;
      IntVector indices;
      _projector->project(ranges, indices, wp_transf.inverse(), *cloud_to_merge);

      //Create aux cloud from ranges
      Cloud2DWithTrajectory* cloudwp = new Cloud2DWithTrajectory;
      for (size_t i=0; i<indices.size(); i++){
	int idx=indices[i];
	if (idx>-1)
	  cloudwp->push_back(cloud_to_merge->at(idx));
      }
      cloudwp->setPose(_cloud_merged->pose());

      //Transform clouds to the waypoint's point of view to get an improved alignment
      //We use an aux cloud since transforming and untransforming cloud_merged was corrupting it
      //Because of numerical rounding errors
      Cloud2DWithTrajectory* aux = new Cloud2DWithTrajectory; 
      _cloud_merged->transform(*aux, wp_transf.inverse());
      cloudwp->transformInPlace(wp_transf.inverse());

      //Align 
      _c2DAligner->setReference(aux);
      _c2DAligner->setCurrent(cloudwp);
      _c2DAligner->compute();
      bool success = _c2DAligner->validate();
      if (success){
	//apply the result of the alignment
	Eigen::Isometry2f result = _c2DAligner->T();
	std::cerr << ">>> Aligning waypoint... result: " << t2v(result).transpose() << std::endl;
	cloudwp->transformInPlace(result);
      }else {
	std::cerr << ">>> failed in aligning waypoint... using initial alignment directly" << std::endl;
      }

      //Transform cloud back to its origin
      cloudwp->transformInPlace(wp_transf);

      //Finally merging waypoint with reference cloud.
      _merger->setMergingDistance(_merging_distance);
      _merger->compute(*_cloud_merged, *cloudwp, wp_transf.inverse());
    
      delete cloudwp;
    }
    
    if (_voxelize_resolution>0) {
      CloudProcessor::voxelize(*_cloud_merged,_voxelize_resolution);
    }
  }*/


  void Cloud2DWithTrajectoryMerger::compute(){

    if (!_reference || !_current){
      std::cerr << "You must provide two clouds" << std::endl;
      return;
    }
    
    
    //We avoid modify the original clouds
    _cloud_merged = new Cloud2DWithTrajectory;
    Cloud2DWithTrajectory* cloud_to_merge = new Cloud2DWithTrajectory;
    *_cloud_merged = *_reference;
    *cloud_to_merge = *_current;

    //Align reference wrt current using transform as i.g.
    Eigen::Isometry2f transform = _current->pose().inverse()*_reference->pose();
    _c2DAligner->setReference(cloud_to_merge);
    _c2DAligner->setCurrent(_cloud_merged);
    _c2DAligner->compute(transform);

    bool success_a = _c2DAligner->validate();
    success_a = false;
    if (success_a){
      //apply the result of the alignment
      Eigen::Isometry2f result_a = _c2DAligner->T();
      _initial_guess = result_a;
    }else {
      //apply the given transform for initial guess
      _initial_guess = transform;
    }
    _cloud_merged->transformInPlace(_initial_guess);
    _cloud_merged->changeTrajectoryOrigin(_cloud_merged->pose()*_initial_guess.inverse());
    _cloud_merged->setPose(_cloud_merged->pose()*_initial_guess.inverse());

    size_t wp_count = _cloud_merged->trajectory().size(); //only merge using wpoints from current
    //Both cloud trajectories expressed in reference origin.
    //Consistent since we did not change cloud_to_merge pose
    _cloud_merged->addTrajectory(cloud_to_merge);
    for (size_t wp = wp_count; wp < _cloud_merged->trajectory().size(); wp++){
      Eigen::Isometry2f wp_transf = _cloud_merged->waypoint(wp);

      //Get cloud points seen from the waypoint.
      FloatVector ranges;
      IntVector indices;
      _projector->project(ranges, indices, wp_transf.inverse(), *cloud_to_merge);

      //Create aux cloud from ranges
      Cloud2DWithTrajectory* cloudwp = new Cloud2DWithTrajectory;
      for (size_t i=0; i<indices.size(); i++){
	int idx=indices[i];
	if (idx>-1)
	  cloudwp->push_back(cloud_to_merge->at(idx));
      }
      cloudwp->setPose(_cloud_merged->pose());

      //Transform clouds to the waypoint's point of view to get an improved alignment
      //We use an aux cloud since transforming and untransforming cloud_merged was corrupting it
      //Because of numerical rounding errors
      Cloud2DWithTrajectory* aux = new Cloud2DWithTrajectory; 
      _cloud_merged->transform(*aux, wp_transf.inverse());
      cloudwp->transformInPlace(wp_transf.inverse());

      //Align 
      _c2DAligner->setReference(aux);
      _c2DAligner->setCurrent(cloudwp);
      _c2DAligner->compute();
      bool success = _c2DAligner->validate();
      if (success){
	//apply the result of the alignment
	Eigen::Isometry2f result = _c2DAligner->T();
	//std::cerr << ">>> Aligning waypoint... result: " << t2v(result).transpose() << std::endl;
	cloudwp->transformInPlace(result);
      }
      //else { std::cerr << ">>> failed in aligning waypoint... using initial alignment directly" << std::endl; }

      //Transform cloud back to its origin
      cloudwp->transformInPlace(wp_transf);

      //Finally merging waypoint with reference cloud.
      _merger->setMergingDistance(_merging_distance);
      _merger->compute(*_cloud_merged, *cloudwp, wp_transf.inverse());

      delete aux;
      delete cloudwp;
    }
    
    if (_voxelize_resolution>0) {
      CloudProcessor::voxelize(*_cloud_merged,_voxelize_resolution);
    }

    //delete temporal clouds
    delete cloud_to_merge;
  }

    void Cloud2DWithTrajectoryMerger::prune(){

    if (!_reference || !_current){
      std::cerr << "You must provide two clouds" << std::endl;
      return;
    }
    
    
    //We avoid modify the original clouds
    _cloud_merged = new Cloud2DWithTrajectory;
    *_cloud_merged = *_reference;
    Cloud2DWithTrajectory* cloud_for_prune = new Cloud2DWithTrajectory;
    *cloud_for_prune = *_current;
    
    //Get relative transform
    Eigen::Isometry2f transform = _reference->pose().inverse()*_current->pose();
    cloud_for_prune->transformInPlace(transform);
    cloud_for_prune->changeTrajectoryOrigin(cloud_for_prune->pose()*transform.inverse());
    cloud_for_prune->setPose(cloud_for_prune->pose()*transform.inverse());

    for (size_t wp = 0; wp < cloud_for_prune->trajectory().size(); wp++){
      Eigen::Isometry2f wp_transf = cloud_for_prune->waypoint(wp);

      //Get cloud points seen from the waypoint.
      FloatVector ranges;
      IntVector indices;
      _projector->project(ranges, indices, wp_transf.inverse(), *cloud_for_prune);

      //Create aux cloud from ranges
      Cloud2DWithTrajectory* cloudwp = new Cloud2DWithTrajectory;
      for (size_t i=0; i<indices.size(); i++){
	int idx=indices[i];
	if (idx>-1)
	  cloudwp->push_back(cloud_for_prune->at(idx));
      }
      cloudwp->setPose(cloud_for_prune->pose());

      //Finally merging waypoint with reference cloud.
      _merger->setMergingDistance(_merging_distance);
      _merger->prune(*_cloud_merged, *cloudwp, wp_transf.inverse());
      
      delete cloudwp;
    }
    
    if (_voxelize_resolution>0) {
      CloudProcessor::voxelize(*_cloud_merged,_voxelize_resolution);
    }

    //delete temporal clouds
    delete cloud_for_prune;
  }

  
}
