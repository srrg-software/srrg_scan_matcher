#pragma once
#include "cloud2d.h"

namespace srrg_scan_matcher {

  /*!
    Class that contains a set of static methods to operate on pairs of clouds.
    It encapsulates function such as merging, voxelization or radial merging
   */
  class CloudProcessor {
  public:
    //! Merges src in dest:
    //! it requires the index image of dest and of src, seen from the same point
    //! and also the depth buffers (ranges).
    //! The clouds should be aligned, then
    //! points that are closer than distanceThreshold are merged based on the scaling values
    //! if the normals are compatible
    static void merge(srrg_core::FloatVector& dest_ranges,
	       srrg_core::IntVector& dest_indices,
	       Cloud2D& dest,
	       srrg_core::FloatVector& src_ranges,
	       srrg_core::IntVector& src_indices,
	       Cloud2D& src, 
	       float normal_threshold = 1,
	       float distance_threshold = 0.2
	       );

    //! Merges the points in dest that fall in the same radial bin
    static void radialMerge(srrg_core::FloatVector& dest_ranges,
			    srrg_core::IntVector& dest_indices,
			    srrg_core::FloatVector& dest_reverse_ranges,
			    srrg_core::IntVector& dest_reverse_indices,
			    Cloud2D& dest,
			    float normal_threshold = 1,
			    float distance_threshold = 0.2);




    //! prunes the points in model, computing a scaled average
    //! one point will survive for each voxel of side res. Res is in meters as it should be
    static void voxelize(Cloud2D& model, float res);  


    //! Similar to merge() but does not add new points
    //! Points dest that have been "traversed" by src are removed.
    static void prune(srrg_core::FloatVector& dest_ranges,
		      srrg_core::IntVector& dest_indices,
		      Cloud2D& dest,
		      srrg_core::FloatVector& src_ranges,
		      srrg_core::IntVector& src_indices,
		      Cloud2D& src,
		      float normal_threshold = 1,
		      float distance_threshold = 0.2);

  };

}
