#pragma once

#include "projector2d.h"

namespace srrg_scan_matcher {

  /*!
     Processing object that merges two clouds
     The usage is the usual one;
     It has to be instantiated with a projector
     that it uses to see which points are visible
     in order to to the merge

     \code{.cpp}
     Merger2D* merger = new Merger2D(new Projector2D);
     merger->compute(destination, source);
     \endcode

     destination is 
     - augmented with the point of source
       that are likely to belong to new objects
     - pruned from points that are "seen through" by the points in source
     - points between the two clouds that are closed are "fused" and their statistics
       updated

   */
  class Merger2D {

  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
    Merger2D(Projector2D* projector_=NULL);
    virtual ~Merger2D();
    inline const Projector2D* projector() const { return _projector; }
    inline void setProjector(Projector2D* projector_) {_projector=projector_;}
    virtual void compute(Cloud2D& dest, Cloud2D& src, const Eigen::Isometry2f& origin = Eigen::Isometry2f::Identity());
    virtual void prune(Cloud2D& reference, Cloud2D& current, const Eigen::Isometry2f& origin = Eigen::Isometry2f::Identity());

    
    inline float mergingDistance() const { return _merging_distance; }
    inline void setMergingDistance(float md)  {_merging_distance = md;}

    inline float mergingNormalAngle() const { return _merging_normal_angle; }
    inline void setMergingNormalAngle(float ma)  {_merging_normal_angle = ma;}


    inline bool projectiveMergeEnabled() const { return _projective_merge_enabled;}
    inline void setEnableProjectiveMerge(bool projective_merge_enabled_) {
      _projective_merge_enabled = projective_merge_enabled_;
    }

  private:
      
    bool _projective_merge_enabled;
    float _merging_distance;
    float _merging_normal_angle;
    Projector2D *_projector;
    
  };
}
