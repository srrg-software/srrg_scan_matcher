#include "cloud2d.h"
#include <string>
#include <stdexcept>
#include <GL/gl.h>
#include "srrg_system_utils/stream_helpers.h"
using namespace std;

namespace srrg_scan_matcher{

  using namespace srrg_core;

  void Cloud2D::add(const Cloud2D& other) {
    if (&other == this) {
      return;
    }

    size_t k = size();
    resize(k + other.size());

    for (size_t i = 0; i < other.size(); ++i) {
      at(k + i) = other.at(i);
    }
  }


  void Cloud2D::transformInPlace(const Eigen::Isometry2f& T) {
    for (size_t i = 0; i < size();++ i) {
      if (at(i).accumulator() <= 0) {
       	throw std::runtime_error("Negative Point Accumulator");
      }

      at(i).transformInPlace(T);
    }
  }

  void Cloud2D::transform(Cloud2D& other, const Eigen::Isometry2f& T) const {
    other.resize(size());

    for (size_t i = 0; i < size(); ++i) {
      other[i] = at(i).transform(T);
    }
  }

  //! clips to a max_range around a pose
  void Cloud2D::clip(float max_range, const Eigen::Isometry2f& pose) {
    Eigen::Isometry2f T = pose.inverse();
    max_range *= max_range;

    int k = 0;
    for (size_t i = 0; i < size(); ++i) {
      const RichPoint2D& p = at(i);
      Eigen::Vector2f other_p = T * p.point();

      if (other_p.squaredNorm() < max_range) {
	at(k) = p;
	k++;
      }
    }

    resize(k);
  }

  void Cloud2D::draw(bool draw_normals,
		     Eigen::Isometry2f T,
		     bool draw_pose_origin, 
		     bool use_fans) const {
    glPushMatrix();
    Eigen::Vector3f t=t2v(T);
    glTranslatef(t.x(),
		 t.y(),
		 0);
    glRotatef(t.z()*180.0f/M_PI,0,0,1);


    glPushAttrib(GL_COLOR|GL_POINT_SIZE);
    glPointSize(5);
    glBegin(GL_POINTS);
    for(size_t i=0; i<size(); i++){
      const RichPoint2D& p=at(i);
      if (p.color().z()==1) {
	glColor3f(p.color().x(), p.color().y(), p.color().z());
	glNormal3f(0,0,1);
	glVertex3f(p.point().x(), p.point().y(), 0);
      } 
    }
    glEnd();
    glPopAttrib();

    glPushAttrib(GL_COLOR|GL_POINT_SIZE);
    glPointSize(2);
    glBegin(GL_POINTS);
    for(size_t i=0; i<size(); i++){
      const RichPoint2D& p=at(i);
      if (p.color().z()!=1) {
	glColor3f(p.color().x(), p.color().y(), p.color().z());
	glNormal3f(0,0,1);
	glVertex3f(p.point().x(), p.point().y(), 0);
      } 
    }
    glEnd();
    glPopAttrib();
    
    glPushAttrib(GL_COLOR);
    if (use_fans) {
      glColor4f(.5, .5, .5, 0.1);
      glBegin(GL_TRIANGLE_FAN);
      glNormal3f(0,0,1);
      glVertex3f(0,0,0);
      for(size_t i=0; i<size(); i++){
	const RichPoint2D& p=at(i);
	glNormal3f(0,0,1);
	glVertex3f(p.point().x(), p.point().y(), 0);
      }
      glEnd();
    } 
    glPopAttrib();


    float nscale=0.1;
    if (draw_normals){
      glPushAttrib(GL_COLOR);
      glBegin(GL_LINES);
      for(size_t i=0; i<size(); i++){
	const RichPoint2D& p=at(i);
	glColor3f(p.color().x(), p.color().y(), p.color().z());
	glNormal3f(0,0,1);
	glVertex3f(p.point().x(), p.point().y(), 0);
	glVertex3f(p.point().x()+p.normal().x()*nscale, p.point().y()+p.normal().y()*nscale, 0);
      }
      glEnd();
      glPopAttrib();
    }
    glPopMatrix();
  }
  

  void Cloud2D::save(ostream& os) const {
    size_t s=size();
    StreamHelpers::writeBinary(os,s);
    for (size_t i=0; i<s; i++){
      RichPoint2D p=at(i);
      p.normalize();
      StreamHelpers::writeBinary(os,p.point().x());
      StreamHelpers::writeBinary(os,p.point().y());
      StreamHelpers::writeBinary(os,p.normal().x());
      StreamHelpers::writeBinary(os,p.normal().y());
      StreamHelpers::writeBinary(os,p.accumulator());
    }
  }
  
  bool Cloud2D::load(istream& is) {
    if (! is.good())
      return false;
    
    cerr << __PRETTY_FUNCTION__ << endl;
    size_t s;
    StreamHelpers::readBinary(is,s);
    cerr << "size is" << s << endl;
    resize(s);
    for (size_t i=0; i<s; i++){
      RichPoint2D p;
      StreamHelpers::readBinary(is,p.point().x());
      StreamHelpers::readBinary(is,p.point().y());
      StreamHelpers::readBinary(is,p.normal().x());
      StreamHelpers::readBinary(is,p.normal().y());
      StreamHelpers::readBinary(is,p._accumulator);
      at(i)=p;
    }
    return (is.good());
  }

  void Cloud2D::computeBoundingBox(Eigen::Vector2f& lower, Eigen::Vector2f& upper) const {
    lower.x()=std::numeric_limits<float>::max();
    lower.y()=std::numeric_limits<float>::max();
    upper.x()=-std::numeric_limits<float>::max();
    upper.y()=-std::numeric_limits<float>::max();
    for (size_t i=0; i<size(); i++) {
      const Eigen::Vector2f& p=at(i).point();
      lower.x() = lower.x() < p.x() ? lower.x() : p.x();
      lower.y() = lower.y() < p.y() ? lower.y() : p.y();
      upper.x() = upper.x() > p.x() ? upper.x() : p.x();
      upper.y() = upper.y() > p.y() ? upper.y() : p.y();
    }
  }

  void Cloud2D::setColor(const Eigen::Vector3f& color) {
    for (size_t i=0; i<size(); i++){
      at(i)._color=color;
    }
  }


  void Cloud2DWithPose::draw(bool draw_normals,
			     bool draw_pose_origin, 
			     bool use_fans) const {
    Cloud2D::draw(draw_normals, _pose, draw_pose_origin, use_fans);
  }

  void Cloud2DWithPose::save(ostream& os) const {
    Cloud2D::save(os);
    Eigen::Vector3f pose = t2v(_pose);
    StreamHelpers::writeBinary(os,pose.x());
    StreamHelpers::writeBinary(os,pose.y());
    StreamHelpers::writeBinary(os,pose.z());
  }
  
  bool Cloud2DWithPose::load(istream& is) {
    Cloud2D::load(is);
    Eigen::Vector3f pose;
    StreamHelpers::readBinary(is,pose.x());
    StreamHelpers::readBinary(is,pose.y());
    StreamHelpers::readBinary(is,pose.z());

    setPose(v2t(pose));
    return (is.good());
  }

}
