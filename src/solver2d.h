#pragma once

#include <iostream>
#include <stdexcept>

#include "cloud2d.h"

namespace srrg_scan_matcher {
  typedef srrg_core::IntPairVector CorrespondenceVector;

  /*!
     Least Squares Solver for SCAN matching;
     Given 
     - two clouds
     - an initial guess for their relatibe transform
     - a vector of correspondences, telling which points in the reference corresponds 
       to which point in the current
     computes the least squares solution that brings them "closer"

     \code{.cpp}
     Solver2d* s=...
     CorrespondenceFinder2D* cfinder=...
     cfinder->setSolver(s);
     s->setReference(ref)
     s->setCurrent(cur);
     cfinder->init();
     
     // optional, tells the system that it will use only a subset of the reference points
     s->setReferencePointsHint(cfinder->indicesReference()); 

     for (int i=0; i< iterations; i++) {
	cfinder->compute(); // comptes the correspondences based on the current transform in the solver;
	s->optimize(cfinder->correspondences());
	cerr << "current chi" << s->chi() << " current transform" << t2v(s->T()).transpose() << endl;
     }
     \endcode

   */

  class Solver2D{
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
    Solver2D();
    virtual ~Solver2D() {}


    //! gives a hint to the system on which omegas need to be recomputed
    //! needs to be called after setting the reference model
    void setReferencePointsHint(const std::vector<int>& reference_points_hint);


    // does one round of optimization: updates the transform T,
    // the error and the number of inliers
    void optimize(const CorrespondenceVector& correspondences, bool update_information=false);

    // setter & getter
    inline const Eigen::Isometry2f& T() const { return _T; }
    inline void setT(const Eigen::Isometry2f T) { _T = T; }
    inline void setPrior(const Eigen::Isometry2f prior_T, 
			 const Eigen::Matrix3f prior_omega=Eigen::Matrix3f::Zero()) { 
      _T = prior_T;
      _prior_T=prior_T;
      _prior_omega=prior_omega;
    }

    // below are the parametersfor the information matrices of tge point and the normal component
    // of one element in the clouds
    inline const Eigen::Matrix2f& flatOmega() const { return _flat_omega; }
    inline void setFlatOmega(const Eigen::Matrix2f& flat_omega) { _flat_omega = flat_omega; }
    inline const Eigen::Matrix2f& longLinearOmega() const { return _long_linear_omega; }
    inline void setLongLinearOmega(const Eigen::Matrix2f& long_linear_omega) { _long_linear_omega = long_linear_omega; }
    inline float damping() const {return _damping;}
    inline void setDamping(float damping_) {_damping=damping_;}
    inline const Cloud2D* reference() const { return _reference; }
    inline void setReference(Cloud2D* reference_) { _reference = reference_; }
    inline const Cloud2D* current() const { return _current; }
    inline void setCurrent(Cloud2D* current) { _current = current; }
    inline float chi() const { return _chi; }
    inline int inliers() const { return _inliers; }
    inline float inliersError() const { return _inliers_error; }

    inline Eigen::Matrix3f informationMatrix() const {return _information_matrix;}
  private:


    // updates and computes the omegas
    // if no argument is given it updates all omegas, otherwise 
    // it updates the omegas of the passed vector of indices
    void computeOmegas(const std::vector<int> update_list = std::vector<int>());

    void errorAndJacobian(const Eigen::Vector2f& reference_pt, const Eigen::Vector2f& reference_normal,
			  const Eigen::Vector2f& current_pt, const Eigen::Vector2f& current_normal,
			  Eigen::Vector2f& pt_error, Eigen::Vector2f& normal_error, srrg_core::Matrix4_3f& J);

    // computes the Hessian and the coefficient vector of the reference points
    // between imin and imax; it also computes the error and the number of inliers
    virtual void linearize(const CorrespondenceVector& correspondences,
			   Eigen::Matrix3f& H, Eigen::Vector3f& b,
			   float& chi, int& inliers, float& inliers_error,
			   size_t imin = 0, size_t imax = std::numeric_limits<size_t>::max());    

    std::vector<bool> _inliers_vector;
    Eigen::Isometry2f _T; // position of the world w.r.t the sensor
    Eigen::Isometry2f _prior_T; // position of the world w.r.t the sensor
    Eigen::Matrix2f _flat_omega, _long_linear_omega;
    Eigen::Matrix3f _information_matrix;
    Cloud2D *_reference, *_current;
    srrg_core::Matrix2fVector _omega_points, _omega_normals;
    float _chi;
    float _max_error;
    float _damping;
    int _inliers;
    float _inliers_error;
    Eigen::Matrix3f _prior_omega;
  };
}


