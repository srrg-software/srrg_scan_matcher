#include "correspondence_finder2d.h"
#include <stdexcept>
#include <GL/gl.h>
namespace srrg_scan_matcher {

  CorrespondenceFinder2D::CorrespondenceFinder2D(Solver2D* solver) {
      _correspondences.clear();
      _solver = solver;
      _max_squared_distance=0.3*0.3;
      _min_normal_cos=cos(M_PI/6);
    }

  void CorrespondenceFinder2D::drawCorrespondences(Eigen::Isometry2f T) const {
    glPushAttrib(GL_COLOR);
    glColor3f(0,1,0);
    _solver->current()->draw(false, T);
    glColor3f(0,0,1);
    _solver->reference()->draw(false);
    glColor3f(1,0,0);
    glBegin(GL_LINES);
    for (size_t i = 0; i < _correspondences.size(); ++i) {
      int ref_idx  = _correspondences[i].first;
      int curr_idx = _correspondences[i].second;

      Eigen::Vector2f pt_curr = T*(*_solver->current())[curr_idx].point();
      Eigen::Vector2f pt_ref = (*_solver->reference())[ref_idx].point();
      glNormal3f(0,0,1);
      glVertex3f(pt_ref.x(), pt_ref.y(),0);
      glNormal3f(0,0,1);
      glVertex3f(pt_curr.x(), pt_curr.y(),0);
    }
    glEnd();
    glPopAttrib();
  }
}

